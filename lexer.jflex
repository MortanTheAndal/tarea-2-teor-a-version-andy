package cup;

import java_cup.runtime.ComplexSymbolFactory;
import java_cup.runtime.ComplexSymbolFactory.Location;
import java_cup.runtime.Symbol;
import java.lang.*;
import java.io.InputStreamReader;

%%

%class Lexer
%implements sym
%public
%unicode
%line
%column
%cup
%char
%{
	

    public Lexer(ComplexSymbolFactory sf, java.io.InputStream is){
		this(is);
        symbolFactory = sf;
    }
	public Lexer(ComplexSymbolFactory sf, java.io.Reader reader){
		this(reader);
        symbolFactory = sf;
    }
    
    private StringBuffer sb;
    private ComplexSymbolFactory symbolFactory;
    private int csline,cscolumn;

    public Symbol symbol(String name, int code){
		return symbolFactory.newSymbol(name, code,
						new Location(yyline+1,yycolumn+1, yychar), // -yylength()
						new Location(yyline+1,yycolumn+yylength(), yychar+yylength())
				);
    }
    public Symbol symbol(String name, int code, String lexem){
	return symbolFactory.newSymbol(name, code, 
						new Location(yyline+1, yycolumn +1, yychar), 
						new Location(yyline+1,yycolumn+yylength(), yychar+yylength()), lexem);
    }
    
    protected void emit_warning(String message){
    	System.out.println("scanner warning: " + message + " at : 2 "+ 
    			(yyline+1) + " " + (yycolumn+1) + " " + yychar);
    }
    
    protected void emit_error(String message){
    	System.out.println("scanner error: " + message + " at : 2" + 
    			(yyline+1) + " " + (yycolumn+1) + " " + yychar);
    }
%}

Newline    = \r | \n | \r\n
Whitespace = [ \t\f] | {Newline}
Number     = [0-9]+


/* comments */
Comment = {TraditionalComment} | {EndOfLineComment}
TraditionalComment = "/*" {CommentContent} \*+ "/"
EndOfLineComment = "//" [^\r\n]* {Newline}
CommentContent = ( [^*] | \*+[^*/] )*

ident = ([:jletter:] | "_" ) ([:jletterdigit:] | [:jletter:] | "_" )*


%eofval{
    return symbolFactory.newSymbol("EOF",sym.EOF);
%eofval}

%state CODESEG

%%  

<YYINITIAL> {

  {Whitespace}              {   /*ignoramos espacios*/    }
  "bajar-pluma"             { return symbolFactory.newSymbol("BAJARPLUMA", BAJARPLUMA); }
  "levantar-pluma"          { return symbolFactory.newSymbol("LEVANTARPLUMA", LEVANTARPLUMA); }
  "color-pluma"             { return symbolFactory.newSymbol("COLORPLUMA", COLORPLUMA); }
  "direccion-pluma"         { return symbolFactory.newSymbol("DIRECCIONPLUMA", DIRECCIONPLUMA); }
  "avanzar"                 { return symbolFactory.newSymbol("AVANZAR", AVANZAR); }
  "if"                      { return symbolFactory.newSymbol("IF", IF); }
  "then"                    { return symbolFactory.newSymbol("THEN", THEN); }
  "{"                       { return symbolFactory.newSymbol("ABRE", ABRE); }
  "}"                       { return symbolFactory.newSymbol("CIERRA", CIERRA); }
  "else"                    { return symbolFactory.newSymbol("ELSE", ELSE); }
  "while"                   { return symbolFactory.newSymbol("WHILE", WHILE); }
  "do"                      { return symbolFactory.newSymbol("DO", DO); }
  ";"                       { return symbolFactory.newSymbol("SEMI", SEMI); }
  "tablero-col"             { return symbolFactory.newSymbol("TABLEROCOL", TABLEROCOL); }
  "borde"                   { return symbolFactory.newSymbol("BORDE", BORDE); }
  "pluma-dir"               { return symbolFactory.newSymbol("PLUMADIR", PLUMADIR); }
  "pluma-col"               { return symbolFactory.newSymbol("PLUMACOL", PLUMACOL); }
  "pluma-arriba"            { return symbolFactory.newSymbol("PLUMAARRIBA", PLUMAARRIBA); }
  "pluma-abajo"             { return symbolFactory.newSymbol("PLUMAABAJO", PLUMAABAJO); }
  "and"                     { return symbolFactory.newSymbol("AND", AND); }
  "or"                      { return symbolFactory.newSymbol("OR", OR); }
  "not"                     { return symbolFactory.newSymbol("NOT", NOT); }
  "A"                       { return symbolFactory.newSymbol("AZUL", AZUL); }
  "R"                       { return symbolFactory.newSymbol("ROJO", ROJO); }
  "V"                       { return symbolFactory.newSymbol("VERDE", VERDE); }
  "B"                       { return symbolFactory.newSymbol("BLANCO", BLANCO); }
  "N"                       { return symbolFactory.newSymbol("N", N); }
  "S"                       { return symbolFactory.newSymbol("SUR", SUR); }
  "E"                       { return symbolFactory.newSymbol("ESTE", ESTE); }
  "O"                       { return symbolFactory.newSymbol("OESTE", OESTE); }
  "0"                       { return symbolFactory.newSymbol("CERO", CERO); }
  "1"                       { return symbolFactory.newSymbol("UNO", UNO); }
  "2"                       { return symbolFactory.newSymbol("DOS", DOS); }
  "3"                       { return symbolFactory.newSymbol("TRES", TRES); }
  "4"                       { return symbolFactory.newSymbol("CUATRO", CUATRO); }
  "5"                       { return symbolFactory.newSymbol("CINCO", CINCO); }
  "6"                       { return symbolFactory.newSymbol("SEIS", SEIS); }
  "7"                       { return symbolFactory.newSymbol("SIETE", SIETE); }
  "8"                       { return symbolFactory.newSymbol("OCHO", OCHO); }
  "9"                       { return symbolFactory.newSymbol("NUEVE", NUEVE); }

  
  

  

  
}



// error fallback
.|\n          { emit_warning("Unrecognized character '" +yytext()+"' -- ignored"); }
