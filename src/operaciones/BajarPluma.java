package operaciones;


import cup.Pluma;

public class BajarPluma implements Operacion {
  private Pluma pluma;
  public BajarPluma(Pluma pluma){
    this.pluma=pluma;

  }
  public void ejecutar(){
    pluma.bajarPluma();
  }

}
