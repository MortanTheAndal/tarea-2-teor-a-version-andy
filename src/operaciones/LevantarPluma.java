package operaciones;


import cup.Pluma;

public class LevantarPluma implements Operacion {
  private Pluma pluma;
  public LevantarPluma(Pluma pluma){
    this.pluma=pluma;

  }
  public void ejecutar(){
    pluma.levantarPluma();
  }

}
