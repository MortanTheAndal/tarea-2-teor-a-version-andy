package operaciones;


public class PuntoYComa implements Operacion{
  private Operacion operador1;
  private Operacion operador2;
  public PuntoYComa(Object op1, Object op2){
    this.operador1= (Operacion) op1;
    this.operador2= (Operacion) op2;
  }
  public void ejecutar(){
    operador1.ejecutar();
    operador2.ejecutar();
  }
}
