package operaciones;


import cup.Pluma;

public class Avanzar implements Operacion {
  private Pluma pluma;
  private Integer pasos;

  public Avanzar (Pluma pluma, Object pasos){
    this.pluma=pluma;
    this.pasos=(Integer) pasos;

  }
  
  public void ejecutar(){
    pluma.avanzar(pasos.intValue());
    pluma.checkBorde();
    
  }

}
