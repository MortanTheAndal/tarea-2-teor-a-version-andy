package operaciones;


import booleanos.Booleano;

public class IfThenElse implements Operacion {
  private Booleano bool;
  private Operacion operador1;
  private Operacion operador2;
  public IfThenElse(Object b, Object op1, Object op2){
    this.bool=(Booleano) b;
    this.operador1=(Operacion) op1;
    this.operador2=(Operacion) op2;

    
  }
  public void ejecutar(){
    if (bool.ejecutar()){operador1.ejecutar();} else{operador2.ejecutar();}
  }

}
