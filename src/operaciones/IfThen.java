package operaciones;


import booleanos.Booleano;

public class IfThen implements Operacion {
  private Booleano bool;
  private Operacion operador;
  public IfThen(Object b, Object op){
    this.bool=(Booleano) b;
    this.operador=(Operacion) op;

    
  }
  public void ejecutar(){
    if (bool.ejecutar()){operador.ejecutar();}
  }
  
}
