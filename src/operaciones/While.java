package operaciones;


import booleanos.Booleano;

public class While implements Operacion{
  Booleano expresion;
  Operacion operador;
  public While(Object b, Object op){
    this.expresion= (Booleano) b;
    this.operador= (Operacion) op;
    
    
  }
  public void ejecutar(){
    while (expresion.ejecutar()){((Operacion) operador).ejecutar();}
  }
}
