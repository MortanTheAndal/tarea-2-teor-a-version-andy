package operaciones;


import cup.Pluma;

public class DireccionPluma implements Operacion {
  private Pluma pluma;
  private Object d;
  public DireccionPluma(Pluma pluma, Object d){
    this.pluma=pluma;
    this.d=d;

    
  }
  public void ejecutar(){
    pluma.setDir((char) d);
  }
}
