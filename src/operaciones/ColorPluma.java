package operaciones;


import cup.Pluma;

public class ColorPluma implements Operacion {
  private Pluma pluma;
  private Object c;
  public ColorPluma(Pluma pluma, Object c){
    this.pluma=pluma;
    this.c=c;

  }
  public void ejecutar(){
    pluma.setColor((char) c);
  }

}
