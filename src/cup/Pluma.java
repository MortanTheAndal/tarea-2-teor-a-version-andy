package cup;


public class Pluma {
  private char[][] tablero;
  private char direccion;
  private char color; 
  private int posx;
  private int posy; 
  private boolean modo; // true = abajo (pintando el tablero);  
  private boolean borde; 
  
  public Pluma(){
    tablero= new char[50][50];
    for ( int i = 0 ; i < 50 ; i++) {
      for ( int j = 0 ; j < 50 ; j++) {
        tablero[i][j]='B';
      }
    }
    direccion = 'E';
    color = 'B';
    posx=0;
    posy=0;
    modo = false; 
    borde = false; 
  } 
  
  //setters 
  public void setDir(char direct){
    direccion = direct; 
  } 
  
  public void setColor(char newColor){
    color = newColor; 
  }
  
  public void levantarPluma(){
    modo = false; 
  }
  
  public void bajarPluma(){
    modo = true; 
  }
  
  public void setPosX(int x){
    posx=x;
  }
  
  public void setPosY(int y){
    posy=y;
  }
  
  public void checkBorde(){
    if ( (getDir()=='E' && getPosX()+1==50) || (getDir()=='O' && getPosX()-1==0)
        || (getDir()=='S' && (getPosY()-1==0)) || (getDir()=='N' && (getPosY()+1==50))){
      borde = true; 
    }
  }
  
  //getters
  public int getPosX(){
    return posx;
  }
  
  public int getPosY(){
    return posy;
  }
  
  public char getDir(){
    return direccion; 
  }
  
  public char getColor(){
    return color;
  }
  
  public char getColorTablero(){
    return tablero[getPosX()][getPosY()];
  }
  
  public boolean getModo(){
    return modo;
  }
  
  public boolean getBorde(){
    checkBorde();
    return borde; 
  }
  
  
  //funciones 
  
  public void avanzar(int pasos){
    
    for ( int p = 0 ; p < pasos ; p++) {
      if (getModo()){
        tablero[getPosX()][getPosY()] = getColor();
      }
      if (getDir()=='E'){
        setPosX(getPosX()+1);
      }
      else if (getDir()=='O'){
        setPosX(getPosX()-1);
      }
      else if (getDir()=='S'){
        setPosY(getPosY()-1);
      }
      else if (getDir()=='N'){
        setPosY(getPosY()+1);
      }
    }
  }
  
  //impresion del tablero
  public void imprimir(){
    StringBuilder sb = new StringBuilder();
    
    for(int i=49; i>=0;i--){
      for (int j=0 ; j<50; j++){
        sb.append(tablero[j][i]);
      }
      sb.append('\n');
    }
    System.out.println(sb);
  }
  
  public static void main(String[] args){
    Pluma p = new Pluma();
    p.setColor('A');
    while (!p.getBorde()){
      p.bajarPluma();
      p.setDir('N');
      p.avanzar(1);
      p.levantarPluma();
      p.setDir('E');
      p.avanzar(1);
      
    }
    p.imprimir();
  }
}
