package booleanos;


import cup.Pluma;

public class PlumaArriba implements Booleano {
  private Pluma pluma;

  public PlumaArriba(Pluma pluma){
    this.pluma=pluma;
  }
  public boolean ejecutar(){
    return !pluma.getModo();
  }

}
