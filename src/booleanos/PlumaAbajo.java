package booleanos;


import cup.Pluma;

public class PlumaAbajo implements Booleano {
  private Pluma pluma;

  public PlumaAbajo(Pluma pluma){
    this.pluma=pluma;
  }
  public boolean ejecutar(){
    return pluma.getModo();
  }
  
}
