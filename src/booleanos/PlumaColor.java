package booleanos;


import cup.Pluma;

public class PlumaColor implements Booleano {

  private Pluma pluma;
  private char color;

  public PlumaColor(Pluma pluma, Object c){
    this.pluma=pluma;
    this.color=(char) c;
  }
  public boolean ejecutar(){
    return pluma.getColor()==color;
  }

}
