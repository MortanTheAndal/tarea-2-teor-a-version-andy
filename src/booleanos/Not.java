package booleanos;


public class Not implements Booleano {
  private Booleano booleano1;

  public Not(Object b1){
    this.booleano1=(Booleano) b1;

  }
  public boolean ejecutar(){
    return !booleano1.ejecutar();
  }

}
