package booleanos;


import cup.Pluma;

public class PlumaDir implements Booleano {
  private Pluma pluma;
  private char direccion;

  public PlumaDir(Pluma pluma, Object d){
    this.pluma=pluma;
    this.direccion=(char) d;
  }
  public boolean ejecutar(){
    return pluma.getDir()==direccion;
  }

}
