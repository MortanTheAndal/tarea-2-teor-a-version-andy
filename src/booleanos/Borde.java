package booleanos;


import cup.Pluma;

public class Borde implements Booleano {
  private Pluma pluma;

  public Borde(Pluma pluma){
    this.pluma=pluma;
  }
  public boolean ejecutar(){
    return pluma.getBorde();
  }
}
