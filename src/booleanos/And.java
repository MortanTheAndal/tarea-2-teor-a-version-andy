package booleanos;


import operaciones.Operacion;

public class And implements Booleano{
  private Booleano booleano1;
  private Booleano booleano2;
  public And(Object b1, Object b2){
    this.booleano1=(Booleano) b1;
    this.booleano2=(Booleano) b2;
  }
  public boolean ejecutar(){
    return booleano1.ejecutar() && booleano2.ejecutar();
  }
}
