package booleanos;


import cup.Pluma;

public class TablaColor implements Booleano {
  private Pluma pluma;
  private char color;

  public TablaColor(Pluma pluma, Object c){
    this.pluma=pluma;
    this.color=(char) c;
  }
  public boolean ejecutar(){
    return pluma.getColorTablero()==color;
  }
}
